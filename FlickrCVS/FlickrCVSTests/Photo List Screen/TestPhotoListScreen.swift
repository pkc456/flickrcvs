//
//  TestPhotoListScreen.swift
//  FlickrCVSTests
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import XCTest
@testable import FlickrCVS

class TestPhotoListScreen: XCTestCase {
    let mockPhotoListViewController: MockPhotoListViewController = MockPhotoListViewController()
    let mockPhotoListPresenter: MockPhotoListPresenter = MockPhotoListPresenter()
    
    func testAttachView() {
        mockPhotoListPresenter.attachView(mockPhotoListViewController)
    }

    func testDetachView() {
        mockPhotoListPresenter.detachView()
    }

    func testFetchPhotoApi() {
        mockPhotoListPresenter.fetchPhotoListApiCall(searchText: "Test")
    }
}

class MockPhotoListPresenter: PhotoListPresenter {
    override func attachView(_ view: IPhotoListView) {
        super.attachView(view)
    }

    override func detachView() {
        super.detachView()
    }

    override func fetchPhotoListApiCall(searchText: String) {
        super.fetchPhotoListApiCall(searchText: searchText)
    }
}
class MockPhotoListViewController: ViewController {
}
