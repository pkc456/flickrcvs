//
//  TestPhotoDetailScreen.swift
//  FlickrCVSTests
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import XCTest
@testable import FlickrCVS

class TestPhotoDetailScreen: XCTestCase {
    let mockPhotoDetailViewController: MockPhotoDetailViewController = MockPhotoDetailViewController()
    let mockPhotoDetailPresenter: MockPhotoDetailPresenter = MockPhotoDetailPresenter()
    let photo = PhotoItem(data: ["" : ""])
    var currentPhoto = PhotoItem(data: ["" : ""])
    
    func testAttachView() {
        mockPhotoDetailPresenter.attachView(mockPhotoDetailViewController, photo: photo)
    }
    
    func testDetachView() {
        mockPhotoDetailPresenter.detachView()
    }

    func testPhotoApi() {
        photo.photoID = "51626451837"
        mockPhotoDetailPresenter.fetchPhotoApiCall(photoId: photo.photoID)
    }

    func testFormattedDateStyle() {
        currentPhoto.publishedDate = nil
        mockPhotoDetailPresenter.setDateInFormattedStyle()
        currentPhoto.publishedDate = "2021-06-24T14:40:14-08:00"
        mockPhotoDetailPresenter.setDateInFormattedStyle()
        currentPhoto.publishedDate = "2021-06-24"
        mockPhotoDetailPresenter.setDateInFormattedStyle()
    }
}

class MockPhotoDetailPresenter: PhotoDetailPresenter {
    override func attachView(_ view: IPhotoDetailView, photo: PhotoItem?) {
        super.attachView(view, photo: photo)
    }
    
    override func detachView() {
        super.detachView()
    }
    
    override func getMediaUrl() -> String? {
        super.getMediaUrl()
    }

    override func fetchPhotoApiCall(photoId: String?) {
        super.fetchPhotoApiCall(photoId: photoId)
    }

    override func setDateInFormattedStyle() {
        super.setDateInFormattedStyle()
    }
}
class MockPhotoDetailViewController: PhotoDetailViewController {
}
