//
//  String+Value.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import Foundation

extension String {

    ///This method ensures that returned value is not nil
    /// - parameter str: input string
    /// - Returns: non-nil string value
    static func ensureNotNil(_ str: Any?) -> String {
        guard let str = str as? String else { return "" }
        return str
    }

    ///This method removes the first forward slash symbol
    /// - Returns: updated string value without first forward slash
    func dropFirstForwardSlash() -> String {
        var input = self
        if input.first == "/" {
            input = String(input.dropFirst())
        }
        return input
    }

    ///This method removes the last forward slash symbol
    /// - Returns: updated string value without last forward slash
    func dropLastForwardSlash() -> String {
        var input = self
        if input.last == "/" {
            input = String(input.dropLast())
        }
        return input
        
    }
}

