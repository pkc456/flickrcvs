//
//  UIStoryboard+Loader.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import Foundation
import UIKit

// MARK: - Storyboard enums
enum Storyboard: String {
    case main = "Main"
}

enum StoryboardIdentifiers: String {
    case photoDetailViewController = "PhotoDetailViewController"
}

// MARK: - Load photo detail view controller
extension UIStoryboard {
    static func loadPhotoDetailViewController(storyboardName: String = Storyboard.main.rawValue,
                                             viewControllerIdentifier identifier: String) -> PhotoDetailViewController? {
        let uiStoryboard = UIStoryboard(name: storyboardName, bundle: nil)
        return uiStoryboard.instantiateViewController(withIdentifier: identifier) as? PhotoDetailViewController
    }
}
