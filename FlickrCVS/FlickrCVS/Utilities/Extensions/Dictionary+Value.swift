//
//  Dictionary+Value.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import Foundation

extension Dictionary {
    
    ///This method returns the string value available at given key path
    /// - parameter keyPath: given key path
    /// - Returns: available string value
    func stringValueAtPath(keyPath: String) -> String {
        let dictionary = self as NSDictionary
        return String.ensureNotNil(dictionary.value(forKeyPath: keyPath))
    }

    ///This method returns the integer value available at given key path
    /// - parameter keyPath: given key path
    /// - Returns: available int value
    func integerValueAtPath(keyPath: String) -> Int {
        let dictionary = self as NSDictionary
        return dictionary.value(forKeyPath: keyPath) as? Int ?? 0
    }
}
