//
//  ConnectivityManager.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import Foundation
import Alamofire

class ConnectivityManager {

    ///This method checks whether user is connected to internet or not
    class var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }

}
