//
//  Constant.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import Foundation

class Constant {

    ///Need this flicker api key for get image size api
    static let flickerApiKey = "0108597317d0ee2bbd22ddc8332c979b"
    static let publishedDateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

    //Information about API can be found here: https://www.flickr.com/services/feeds/docs/photos_public/
    struct Api {
        static let baseURL = "https://www.flickr.com/services/"
        static let getImages = "feeds/photos_public.gne?format=json&nojsoncallback=1&tags=%@"
        static let getSizes = "rest?method=flickr.photos.getSizes&api_key=%#&photo_id=%@&format=json&nojsoncallback=1"
    }
}
