//
//  ViewController.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import UIKit

///This class is responsible to showing image grid view
class ViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var photoCollectionView: UICollectionView!
    var presenter: PhotoListPresenter = PhotoListPresenter()
    let collectionCellIdentifier = "PhotoListCollectionViewCell"

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }

    ///This method will do the required initial steps to set the view
    private func initialSetup() {
        presenter.attachView(self)
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        photoCollectionView.collectionViewLayout.invalidateLayout()
    }

    ///This method hides the search keyboard
    private func hideKeyboard() {
        searchBar.resignFirstResponder()
    }
}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.photoList?.items.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionCellIdentifier, for: indexPath) as! PhotoListCollectionViewCell
        cell.updateUI(photo: presenter.photoList?.items[indexPath.row])
        return cell
    }
}

extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let controller = UIStoryboard.loadPhotoDetailViewController(viewControllerIdentifier: StoryboardIdentifiers.photoDetailViewController.rawValue) {
            hideKeyboard()
            controller.photo = presenter.photoList?.items[indexPath.row]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/3.5, height: collectionView.frame.size.height/4)
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.fetchPhotoListApiCall(searchText: searchText)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        hideKeyboard()
    }
}

extension ViewController: IPhotoListView {
    func reloadCollectionView() {
        DispatchQueue.main.async {
            self.photoCollectionView.reloadData()
        }        
    }
    
    func showInvalidSearchAlert() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error",
                                          message: "Search result failed",
                                          preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok",
                                         style: .default,
                                         handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }

    func showInvalidApiResponseAlert() {
        DispatchQueue.main.async {            
            let alert = UIAlertController(title: "Error",
                                          message: "Model is not created properly",
                                          preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok",
                                         style: .default,
                                         handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }

    func showNoInternetConnectionAlert() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error",
                                          message: "No internet connection",
                                          preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok",
                                         style: .default,
                                         handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
}
