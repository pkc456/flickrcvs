//
//  PhotoListService.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import Foundation
import Alamofire

class PhotoListService {
    
    ///This method call api to fetch Flickr photos
    /// - parameter searchText: search string
    /// - parameter onCompletion: Returns flag for api success, photo list model
    func getPhotoList(searchText: String,
                      onCompletion: @escaping (Bool, PhotoList?) -> Void) {
        
        let getImageSuburl = Constant.Api.getImages.replacingOccurrences(of: "%@", with: searchText)
        let url = Constant.Api.baseURL + ((getImageSuburl).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")

        AF.request(url).response { response in
            switch response.result {
            case .success(let data):
                if let responseData = data {
                    do {
                        let jsonObject = try JSONSerialization.jsonObject(with: responseData)
                        if jsonObject is [String: Any] {
                            let model = PhotoList(data: jsonObject as! [String : Any])
                            onCompletion(true, model)
                        } else {
                            onCompletion(true, nil)
                        }                        
                    } catch {
                        onCompletion(false, nil)
                    }
                } else {
                    onCompletion(false, nil)
                }
            case .failure(_):
                onCompletion(false, nil)
            }
        }
    }
}
