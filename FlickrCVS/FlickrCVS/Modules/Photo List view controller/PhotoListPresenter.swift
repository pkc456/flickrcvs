//
//  PhotoListPresenter.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import Foundation

class PhotoListPresenter: IPhotoListPresenter {
    private var photoListView: IPhotoListView?
    var photoList: PhotoList?

    func attachView(_ view: IPhotoListView) {
        photoListView = view
        startFlow()
    }

    func detachView() {
        photoListView = nil
    }
    
    ///This method starts the presenter's flow
    private func startFlow() {
        fetchPhotoListApiCall(searchText: "")
    }

    func fetchPhotoListApiCall(searchText: String) {
        if ConnectivityManager.isConnectedToInternet {
            PhotoListService().getPhotoList(searchText: searchText) { [weak self] (isSuccess, photoItemModel) in
                guard let self = self else { return }
                if isSuccess {
                    if let model = photoItemModel {
                        self.photoList = model
                        self.photoListView?.reloadCollectionView()
                        self.validatePhotoTitleInModel(modelFromApi: &self.photoList)
                        self.updatePhotoIdentifierInModel(modelFromApi: &self.photoList)
                    } else {
                        self.photoListView?.showInvalidApiResponseAlert()
                    }
                } else {
                    self.photoListView?.showInvalidSearchAlert()
                }
            }
        } else {
            self.photoListView?.showNoInternetConnectionAlert()
        }
        
    }

    ///This method check for empty/nil string in title property. For empty search, the title for some images are coming as " " from api. It is neither nil nor blank, so need to add below conditions.
    /// - parameter modelFromApi: Photo list model instance
    private func validatePhotoTitleInModel(modelFromApi: inout PhotoList?) {
        if let data = modelFromApi {
            for item in data.items {
                var title = "Photo detail"
                if item.title != nil && !(item.title?.isEmpty ?? false) && item.title != " "{
                    title = item.title ?? title
                }
                item.title = title
            }
        }
        
    }

    ///This method update the photo identifier for individual item
    /// - parameter modelFromApi: Photo list model instance
    private func updatePhotoIdentifierInModel(modelFromApi: inout PhotoList?) {
        if let data = modelFromApi {
            for item in data.items {

                let link = item.link?.dropLastForwardSlash()
                
                let index = link?.lastIndex(of: "/")
                if let indexValue = index {
                    let identifier = String((link?[indexValue...]) ?? "")
                    item.photoID = identifier.dropFirstForwardSlash()
                }
            }
        }
    }

}
