//
//  PhotoItem.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import Foundation

///This class holds the single photo item model object
class PhotoItem: NSObject {
    
    // MARK: - Variables
    var title: String?
    var link: String?
    var thumbnail: String?
    var hdPhoto: String?
    var detail: String?
    var dateTaken: String?
    var publishedDate:String?
    var publishDateFormatted: String?
    var author: String?
    var authorID: String?
    var tags: String?
    var photoID: String?
    var height: Int?
    var width: Int?
    
    
    // MARK: - Initializer
    /// This is the initializer method which initiate the class
    /// - parameter data: input json data
    init(data: [String: Any]) {
        super.init()
        title = data.stringValueAtPath(keyPath: "title")
        link = data.stringValueAtPath(keyPath: "link")
        dateTaken = data.stringValueAtPath(keyPath: "date_taken")
        detail = data.stringValueAtPath(keyPath: "description")
        publishedDate = data.stringValueAtPath(keyPath: "published")
        author = data.stringValueAtPath(keyPath: "author")
        authorID = data.stringValueAtPath(keyPath: "author_id")
        tags = data.stringValueAtPath(keyPath: "tags")

        if let image = data["media"] as? Dictionary<String, String>{
            hdPhoto = image.stringValueAtPath(keyPath: "m")
            thumbnail = hdPhoto?.replacingOccurrences(of: "_m", with: "_q")
            //Check various sizes offered on Flickr: https://www.flickr.com/services/api/misc.urls.html
        }
    }
}
