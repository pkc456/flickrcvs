//
//  PhotoList.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import Foundation

///This class holds the photo list model object
class PhotoList: NSObject {
    
    // MARK: - Variables
    var title: String?
    var link: String?
    var detail: String?
    var modifiedTime: String?
    var generator: String?
    var items: [PhotoItem] = []

    // MARK: - Initializer
    /// This is the initializer method which initiate the class
    /// - parameter data: input json data
    init(data: [String: Any]) {
        title = data.stringValueAtPath(keyPath: "title")
        link = data.stringValueAtPath(keyPath: "link")
        detail = data.stringValueAtPath(keyPath: "description")
        modifiedTime = data.stringValueAtPath(keyPath: "modified")
        generator = data.stringValueAtPath(keyPath: "generator")

        if let photoList = data["items"] as? [[String: Any]] {
            for item in photoList {
                let value = PhotoItem(data: item)
                items.append(value)
            }
        }
    }

}
