//
//  PhotoListProtocol.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import Foundation

protocol IPhotoListPresenter {
    
    /// Method to attach IPhotoListView with presenter
    /// - parameter view: IPhotoListView instance
    func attachView(_ view: IPhotoListView)
    
    /// Method to detach ListView from Presenter
    func detachView()
    
    /// This method call fetch photo list Flickr api
    /// - parameter searchText: search string
    func fetchPhotoListApiCall(searchText: String)
}

protocol IPhotoListView {
    /// This method reload collection view after getting success from api
    func reloadCollectionView()
    
    ///This method shows the error alert when api fails for some reason
    func showInvalidSearchAlert()
    
    ///This method shows the error alert when json response is not in correct form
    func showInvalidApiResponseAlert()

    ///This method shows the error alert when no internet connection is available
    func showNoInternetConnectionAlert()
}
