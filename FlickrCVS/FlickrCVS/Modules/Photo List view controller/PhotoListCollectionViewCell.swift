//
//  PhotoListCollectionViewCell.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import UIKit
import Alamofire

class PhotoListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageviewItem: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setPlaceholderImage()
    }
    
    ///This method update the UI
    /// - parameter photo: Photo item model instance
    func updateUI(photo: PhotoItem?) {
        guard let photoUrl = photo?.thumbnail else { return setPlaceholderImage() }
        updateItemImage(thumbnailUrl: photoUrl)
        setAccessibility(text: photo?.title)
    }

    ///This method sets the accessibility for view
    func setAccessibility(text: String?) {
        self.isAccessibilityElement = true
            self.accessibilityLabel = text
    }

    ///This method sets the image form provided url
    /// - parameter thumbnailUrl: url of photo
    private func updateItemImage(thumbnailUrl: String) {
        AF.request(thumbnailUrl).response {response in
            DispatchQueue.main.async {
                switch response.result {
                case .success(let resonseData):
                    if let data = resonseData {
                        self.imageviewItem.image = UIImage(data: data)
                    }
                case .failure(_):
                    self.setPlaceholderImage()
                }
            }
        }
    }

    ///This method sets the placeholder image
    private func setPlaceholderImage() {
        imageviewItem.image = UIImage(named: "Thumbnail")
    }
}
