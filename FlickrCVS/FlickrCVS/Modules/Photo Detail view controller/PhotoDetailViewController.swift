//
//  PhotoDetailViewController.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import UIKit

///This clss is responsible for showing photo detail
class PhotoDetailViewController: UIViewController {
    
    // MARK: - Variables
    @IBOutlet weak var tableviewPhoto: UITableView!
    var presenter: PhotoDetailPresenter = PhotoDetailPresenter()
    var photo: PhotoItem?
    private let detailTableCellIdentifier = "DetailTableViewCell"
    private let detailMediaTableCellIdentifier = "DetailMediaTableViewCell"

    // MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }

    ///This method will do the required initial steps to set the view
    private func initialSetup() {
        presenter.attachView(self, photo: photo)
        self.title = photo?.title
        setShareButton()
    }

    ///This method set the right navigation bar button
    private func setShareButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action,
                                                                 target: self,
                                                                 action: #selector(shareButtonAction))
    }

    ///This method gets callback when user select share button
    @objc func shareButtonAction() {
        if let author = photo?.author, let url = photo?.hdPhoto, let date = photo?.publishDateFormatted {
            let items = ["\(author) posted a photo on \(date). Check it out: \(url)"]
            let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
            present(ac, animated: true)
        }
    }
}

extension PhotoDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: detailMediaTableCellIdentifier,
                                                     for: indexPath) as! DetailMediaTableViewCell
            cell.updateUI(photoUrl: presenter.getMediaUrl(), photo: photo)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: detailTableCellIdentifier,
                                                     for: indexPath) as! DetailTableViewCell
            cell.updateUI(heading: "Author: ", value: photo?.author)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: detailTableCellIdentifier,
                                                     for: indexPath) as! DetailTableViewCell
            let dateValue = photo?.publishDateFormatted != nil ? photo?.publishDateFormatted : photo?.publishedDate
            cell.updateUI(heading: "Published date: ", value: dateValue)
            return cell
        default:
            return UITableViewCell()
        }
        
    }
}

extension PhotoDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}

extension PhotoDetailViewController: IPhotoDetailView {
    func updatePhotoSizeCell(model: PhotoItem?) {
        DispatchQueue.main.async{
            self.photo = model
            self.tableviewPhoto.reloadData()
        }
    }
}
