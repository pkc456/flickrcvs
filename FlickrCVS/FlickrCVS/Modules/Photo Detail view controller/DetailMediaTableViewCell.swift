//
//  DetailMediaTableViewCell.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import UIKit
import Alamofire

class DetailMediaTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewMedia: UIImageView!
    @IBOutlet weak var labelSize: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        enableDynamicType()
        setPlaceholderImage()
    }

    ///This method enables the support of dynamic font
    private func enableDynamicType() {
        labelSize.font = .preferredFont(forTextStyle: .caption1)
        labelSize.adjustsFontForContentSizeCategory = true
    }

    ///This method updates the UI
    /// - parameter photoUrl: url of photo
    /// - parameter photo: Photo item model object
    func updateUI(photoUrl: String?, photo: PhotoItem?) {
        if let width = photo?.width, let height = photo?.height {
            labelSize.text = " \(width)*\(height) "
            setAccessibility()
        }

        if let url = photoUrl {
            setItemImage(photoUrl: url)
        } else {
            setPlaceholderImage()
        }
    }

    ///This method sets the accessibility for view
    func setAccessibility() {
        self.isAccessibilityElement = true
        if let size = labelSize.text {
            self.accessibilityLabel = "Photo frame is \(size)"
        }
    }

    ///This method sets the placeholder image
    private func setPlaceholderImage() {
        imageViewMedia.image = UIImage(named: "Thumbnail")
    }

    ///This method sets the image form provided url
    /// - parameter photoUrl: url of photo
    private func setItemImage(photoUrl: String) {
        AF.request(photoUrl).response {response in
            DispatchQueue.main.async {
                switch response.result {
                case .success(let resonseData):
                    if let data = resonseData {
                        self.imageViewMedia.image = UIImage(data: data)
                    }
                case .failure(_):
                    self.setPlaceholderImage()
                }
            }
        }
    }
}
