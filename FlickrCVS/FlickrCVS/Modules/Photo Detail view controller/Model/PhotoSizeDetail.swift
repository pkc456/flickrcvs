//
//  PhotoSizeDetail.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import Foundation

// MARK: - PhotoSizeDetail model class

class PhotoSizeDetail: NSObject {
    var sizes: Sizes?
    
    /// This is the initializer method which initiate the class
    /// - parameter data: input json data
    init(data: [String: Any]) {
        if let size = data["sizes"] as? [String: Any] {
            sizes = Sizes(data: size)
        }
    }
}


// MARK: - Sizes model class
class Sizes {
    var size: [Size] = []
    
    /// This is the initializer method which initiate the class
    /// - parameter data: input json data
    init(data: [String: Any]) {
        if let response = data["size"] as? [[String: Any]] {
            for item in response {
                let value = Size(data: item)
                size.append(value)
            }
        }
    }
}

// MARK: - Size model class
class Size {
    var label: String?
    var width: Int?
    var height: Int?
    var source: String?
    var url: String?

    /// This is the initializer method which initiate the class
    /// - parameter data: input json data
    init(data: [String: Any]) {
        label = data.stringValueAtPath(keyPath: "label")
        width = data.integerValueAtPath(keyPath: "width")
        height = data.integerValueAtPath(keyPath: "height")
        source = data.stringValueAtPath(keyPath: "source")
        url = data.stringValueAtPath(keyPath: "url")
    }
}

