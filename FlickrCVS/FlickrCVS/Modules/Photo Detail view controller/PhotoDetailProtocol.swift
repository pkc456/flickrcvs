//
//  PhotoDetailProtocol.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import Foundation

/// Protocol to be implemented by photo detail presenter class
protocol IPhotoDetailPresenter {
    
    /// Method to attach IPhotoDetailView with presenter
    /// - parameter view: IPhotoDetailView instance
    func attachView(_ view: IPhotoDetailView, photo: PhotoItem?)
    
    /// Method to detach DetailView from presenter
    func detachView()

    ///This method returns the media URL
    func getMediaUrl() -> String?
        
}

/// Protocol to be implemented by photo detail view controller class
protocol IPhotoDetailView {

    /// This method is responsible to update photo dimensions
    /// - parameter model: Photo item model instance
    func updatePhotoSizeCell(model: PhotoItem?)
}
