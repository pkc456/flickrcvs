//
//  PhotoDetailService.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import Foundation
import Alamofire

class PhotoDetailService {
    
    ///This method call api to fetch different photo resolutions
    /// - parameter photoID: photo identifier whom different resolutions are needed
    /// - parameter onCompletion: Returns flag for api success, photo size model
    ///Detail of this api can be found at https://www.flickr.com/services/api/explore/flickr.photos.getSizes
    func getPhotoSize(photoID: String,
                      onCompletion: @escaping (Bool, PhotoSizeDetail?) -> Void) {
        var getBaseSuburl = Constant.Api.getSizes.replacingOccurrences(of: "%#", with: Constant.flickerApiKey)
        getBaseSuburl = getBaseSuburl.replacingOccurrences(of: "%@", with: photoID)
        let url = Constant.Api.baseURL + ((getBaseSuburl).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")

        AF.request(url).response { response in
            switch response.result {
            case .success(let data):
                if let responseData = data {
                    do {
                        let jsonObject = try JSONSerialization.jsonObject(with: responseData)
                        if jsonObject is [String: Any] {
                            let model = PhotoSizeDetail(data: jsonObject as! [String : Any])
                            onCompletion(true, model)
                        } else {
                            onCompletion(false, nil)
                        }
                    } catch {
                        onCompletion(false, nil)
                    }
                } else {
                    onCompletion(false, nil)
                }
            case .failure(_):
                onCompletion(false, nil)
            }
        }
    }
}
