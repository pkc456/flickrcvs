//
//  PhotoDetailPresenter.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import Foundation

class PhotoDetailPresenter: IPhotoDetailPresenter {
    
    private var photoDetailView: IPhotoDetailView?
    var photoSizeDetailModel: PhotoSizeDetail?
    var currentPhoto: PhotoItem?
    
    func attachView(_ view: IPhotoDetailView, photo: PhotoItem?) {
        photoDetailView = view
        currentPhoto = photo
        startFlow(photoId: currentPhoto?.photoID)
    }
    
    func detachView() {
        photoDetailView = nil
    }
    
    func getMediaUrl() -> String? {
        return (currentPhoto?.hdPhoto != nil) ? currentPhoto?.hdPhoto : currentPhoto?.thumbnail
    }

    ///This method starts the presenter's flow
    /// - parameter photoId: photo identifier
    private func startFlow(photoId: String?) {
        setDateInFormattedStyle()
        fetchPhotoApiCall(photoId: photoId)
    }

    ///This method communicates with api service class and call fetch photo size api
    /// - parameter photoId: photo identifier
    func fetchPhotoApiCall(photoId: String?) {
        if ConnectivityManager.isConnectedToInternet {
            if let photo = photoId {
                PhotoDetailService().getPhotoSize(photoID: photo) { [weak self] (isSuccess, model) in
                    guard let self = self else { return }
                    if isSuccess {
                        self.photoSizeDetailModel = model
                        self.setWidthOfPhoto()
                        self.setHeightOfPhoto()
                        self.photoDetailView?.updatePhotoSizeCell(model: self.currentPhoto)
                    }
                }
            }
        }
    }

    ///This method sets the width of current photo
    private func setWidthOfPhoto() {
        if let model = photoSizeDetailModel, let size = model.sizes {
            for item in size.size {
                if item.source == currentPhoto?.hdPhoto {
                    currentPhoto?.width = item.width
                    break
                }
            }
        }
    }

    ///This method sets the height of current photo
    private func setHeightOfPhoto() {
        if let model = photoSizeDetailModel, let size = model.sizes {
            for item in size.size {
                if item.source == currentPhoto?.hdPhoto {
                    currentPhoto?.height = item.height
                    break
                }
            }
        }
    }

    ///This method sets the date in user readable format
    func setDateInFormattedStyle() {
        if let date = currentPhoto?.publishedDate {
            let formatter = DateFormatter()
            formatter.dateFormat = Constant.publishedDateFormat

            if let dateObject = formatter.date(from: date) {
                formatter.timeStyle = .medium
                formatter.dateStyle = .full
                currentPhoto?.publishDateFormatted = formatter.string(from: dateObject)
            }
        }
    }
}
