//
//  DetailTableViewCell.swift
//  FlickrCVS
//
//  Created by Pardeep Kumar on 2021-10-25.
//

import UIKit

class DetailTableViewCell: UITableViewCell {
    @IBOutlet weak var labelHeading: UILabel!
    @IBOutlet weak var labelValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        enableDynamicType()
    }

    ///This method updates the UI
    /// - parameter heading: heading string
    /// - parameter value: value string
    func updateUI(heading: String, value: String?) {
        labelHeading.text = heading
        labelValue.text = value
        setAccessibility()
    }

    ///This method enables the support of dynamic font
    private func enableDynamicType() {
        labelHeading.font = .preferredFont(forTextStyle: .caption1)
        labelValue.font = .preferredFont(forTextStyle: .caption1)

        labelHeading.adjustsFontForContentSizeCategory = true
        labelValue.adjustsFontForContentSizeCategory = true
    }

    ///This method sets the accessibility for view
    func setAccessibility() {
        self.isAccessibilityElement = true
        if let heading = labelHeading.text, let value = labelValue.text {
            self.accessibilityLabel = "\(heading) is \(value)"
        }
    }
}
