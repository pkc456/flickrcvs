# FlickrCVS
FlickerCVS is an iPhone application that allows a user to search Flickr for images. 

## API 
1. Fetch the list of images using this API from Flickr:
https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1&tags=river
2. Get different image size from Flickr: 
https://www.flickr.com/services/rest?method=flickr.photos.getSizes&api_key=api_key&photo_id=photo_id&format=json&nojsoncallback=1

## Dependency manager
I am using [`Alamofire`](https://github.com/Alamofire/Alamofire) for api request and dowloading images

## Technology stack:
1. Xcode Version 12.2 (12B45b)
2. Swift 5
3. MVP Architecture

## Architecture details:
    View controller is responsible to show data.
    Presenter is responsible for business conditions and communicating with api call.
    Model persist data.

1. In view controller's `viewDidLoad` method, presenter is initialized. Presenter calls the api, parse the model data and validate it. Then, presenter calls view controller's appropriate method (using protocol)
2. For business conditions which are written in presenter, I also write test cases `XCTest`
3. For listing of screen, I am showing the thumbnail photo from `flickr.photos.getSizes` api, not full size image
4. At photo detail screen, I am showing the bigger size image
5. For some photos, title is coming as empty string. To handle this, I have written a business logic in method named `validatePhotoTitleInModel(modelFromApi` at `PhotoListPresenter.swift`
6. When user changes orientation and search string together, the collection view cell size is not updating automatically. To handle this edge case, I use `viewWillTransition` method
7. Internet connectivity is checked(`isConnectedToInternet`) before calling an api. If no internet connection is available, then showing a appropriate alert message to user
8. Method and parameter commenting is completed in detail
9. Added accessibility (VoiceOver support)

## Next scope
1. Splash screen can be customized
2. Show loader while api is called
3. Customize photo detail table cells

## Smart decisions
1. On photo list screen, I am loading thumbnail not full size photo.
2. On photo list screen's API `(feeds/photos_public.gne)`, the name of photo is coming empty for some photos and photo identifier is absent. To make sure that model properties contains correct values, I have written business conditions at presenter class. These business conditions run in secondary threads after reloading the photo collection view. Thus, user will be able to see the photo list instantaneously and model updations are done in background without blocking main thread.

